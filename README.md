<!-- Emacs: -*- coding: utf-8; eval: (auto-fill-mode -1); eval: (visual-line-mode t) -*- -->

# Présentation de R &mdash; Code réel, objets virtuels

[*Présentation de R &mdash; Code réel, objets virtuels*](https://gitlab.com/vigou3/presentation-r/-/releases/) est un laboratoire (ou atelier) de prise en main de R et d'initiation à la stratégie de travail «le code est réel, les objets sont virtuels» (la [première philosophie d'utilisation de ESS](http://ess.r-project.org/Manual/ess.html#Philosophies-for-using-ESS_0028R_0029)). Le laboratoire est offert dans le cadre du cours [IFT-1902 Programmation avec R pour l'analyse de données](https://www.ulaval.ca/les-etudes/cours/repertoire/detailsCours/ift-1902-informatique-pour-actuaires.html) à l'[École d'actuariat](https://www.act.ulaval.ca) de l'[Université Laval](https://ulaval.ca).

L'approche dite de «code réel, objets virtuels» considère qu'il importe de conserver, au terme d'une session de travail, non pas tant les objets créés durant la séance que le code qui a servi à les créer. Ainsi, on sauvegarde dans des *fichiers de script* nos expressions R et le code de nos fonctions personnelles. Les objets sont ensuite créés au besoin en exécutant le code des fichiers de script.

## Auteur

Vincent Goulet, professeur titulaire, École d'actuariat, Université Laval

## Licence

«Présentation de R &mdash; Code réel, objets virtuels» est mis à disposition sous licence [Attribution-Partage dans les mêmes conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) de Creative Commons.

Consulter le fichier `LICENSE` pour la licence complète.

## Modèle de développement

Le processus de rédaction et de maintenance du projet suit le modèle [*Gitflow Workflow*](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow ). Seule particularité: la branche *master* se trouve dans le dépôt [`presentation-r`](https://gitlab.com/vigou3/presentation-r) dans GitLab, alors que la branche de développement se trouve dans le dépôt [`presentation-r-devel`](https://projets.fsg.ulaval.ca/git/scm/vg/presentation-r-devel) dans le serveur BitBucket de la Faculté des sciences et de génie de l'Université Laval.

Prière de passer par le dépôt `presentation-r-devel` pour proposer des modifications; consulter le fichier `CONTRIBUTING.md` pour la marche à suivre.
