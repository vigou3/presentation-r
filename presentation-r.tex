%%% Copyright (C) 2021-2024 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet «Présentation de R - Code réel,
%%% objets virtuels»
%%% https://gitlab.com/vigou3/presentation-r
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% http://creativecommons.org/licenses/by-sa/4.0/

\documentclass[aspectratio=169,10pt,xcolor=x11names,english,french]{beamer}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage{fontawesome5}              % icônes \fa*
  \usepackage{awesomebox}                % \tipbox et autres
  \usepackage{changepage}                % page licence
  \usepackage{dirtree}                   % arbre de fichiers
  \usepackage{tabularx}                  % page licence
  \usepackage{relsize}                   % \smaller et al.
  \usepackage{listings}                  % code source
  \usepackage{framed}                    % env. leftbar
  \usepackage[overlay,absolute]{textpos} % couvertures
  \usepackage{metalogo}                  % logo \XeLaTeX

  %% ============================
  %%  Information de publication
  %% ============================
  \title{Présentation de R - Code réel, objets virtuels}
  \author{Vincent Goulet}
  \renewcommand{\year}{2024}
  \renewcommand{\month}{08}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/presentation-r}

  %%% ===================
  %%%  Style du document
  %%% ===================

  %% Thème Beamer
  \usetheme{metropolis}

  %% Polices de caractères
  \setsansfont{Fira Sans Book}
  [
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}
  ]
  \newfontfamily\titlefontOS{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = OldStyle
  ]
  \newfontfamily\titlefontFC{FiraSans}
  [
    Extension = .otf,
    UprightFont = *-Book,
    BoldFont = *-SemiBold,
    BoldItalicFont = *-SemiBoldItalic,
    Scale = 1.0,
    Numbers = Uppercase
  ]
  \usepackage[babel=true]{microtype}

  %% Couleurs
  \definecolor{comments}{rgb}{0.7,0,0} % commentaires
  \definecolor{link}{rgb}{0,0.4,0.6}   % liens internes
  \definecolor{url}{rgb}{0.6,0,0}      % liens externes
  \colorlet{codebg}{LightYellow1}      % fond code R
  \definecolor{rouge}{rgb}{0.9,0,0.1}  % bandeau rouge UL
  \definecolor{or}{rgb}{1,0.8,0}       % bandeau or UL
  \colorlet{alert}{mLightBrown}        % alias de couleur Metropolis
  \colorlet{dark}{mDarkTeal}           % alias de couleur Metropolis
  \colorlet{code}{mLightGreen}         % alias de couleur Metropolis
  \colorlet{metropolisbg}{black!2}     % couleur de fond Metropolis
  \colorlet{shadecolor}{codebg}

  %% Hyperliens
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {Présentation de R --- Code réel, objets virtuels},
    colorlinks = {true},
    linktocpage = {true},
    allcolors = {link},
    urlcolor = {url},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit},
    bookmarksopen = {true},
    bookmarksnumbered = {true},
    bookmarksdepth = {subsection}}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% Sections de code source
  \lstloadlanguages{R}
  \lstset{language=R,
    basicstyle=\small\ttfamily\NoAutoSpacing,
    keywordstyle=\mdseries,
    commentstyle=\color{comments}\slshape,
    emphstyle=\bfseries,
    escapeinside=`',
    extendedchars=true,
    showstringspaces=false,
    backgroundcolor=\color{codebg},
    frame=leftline,
    framerule=2pt,
    framesep=5pt,
    xleftmargin=7.4pt
  }

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Renvois vers vidéos YouTube
  \newcommand{\video}[2]{%
    \begin{center}
      \href{#1}{%
        \makebox[5mm]{\raisebox{-2pt}{\Large\faYoutube}}\;{#2}}
    \end{center}}

  %% Lien vers GitLab dans la page de notices
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %% Identification de la licence CC BY-SA.
  \newcommand{\ccbysa}{\mbox{%
    \faCreativeCommons\kern0.1em%
    \faCreativeCommonsBy\kern0.1em%
    \faCreativeCommonsSa}~\faCopyright[regular]\relax}

  %% Hyperlien avec symbole de lien externe juste après; second
  %% argument peut être vide pour afficher l'url comme lien
  %% [https://tex.stackexchange.com/q/53068/24355 pour procédure de
  %% test du second paramètre vide]
  \newcommand{\link}[2]{%
    \def\param{#2}%
    \ifx\param\empty
      \href{#1}{\nolinkurl{#1}~\smaller\faExternalLink*}%
    \else
      \href{#1}{#2~\smaller\faExternalLink*}%
    \fi
  }

  %% Noms de fonctions, code, environnement, etc.
  \newcommand{\code}[1]{\textcolor{code}{\texttt{#1}}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Longueurs pour la composition des pages couvertures avant et
  %% arrière.
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}
  \newlength{\logoheight}

\begin{document}

%% frontmatter
\include{couverture-avant}
\include{notices}

\begin{frame}
  \frametitle{Sommaire}
  \tableofcontents
\end{frame}

%% mainmatter
\section{Introduction}

\begin{frame}
  \frametitle{Description sommaire}

  «\emph{R is a free software environment for statistical computing and graphics}»
  \bigskip

  \begin{itemize}
  \item Environnement intégré de manipulation de données, de calcul et
    de préparation de graphiques
    \begin{itemize}
    \item créé spécifiquement pour l'analyse de données
    \end{itemize}
  \item Aussi un langage de programmation complet
    \begin{itemize}
    \item fonctionnalités statistiques de R programmées en R
    \end{itemize}
  \item Langage \alert{interprété} (et non \alert{compilé})
    \begin{itemize}
    \item utilisation interactive par le biais d'un interpréteur
    \end{itemize}
  \end{itemize}

  \video{https://youtu.be/FnoMjJ9Knl0}{Principales caractéristiques du langage R}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{\faCogs\; Prise en main (partie I)}

  \begin{enumerate}
  \item Ouvrir une ligne de commande (Terminal ou Git~Bash)
  \item Changer le répertoire courant pour un répertoire de votre
    choix (avec la commande \code{cd})
  \item Lancer R depuis l'invite de commande \\
    \medskip
    \begin{minipage}[t]{0.65\linewidth}
      \small Windows (remplacer \texttt{x.y.z} par le numéro de version de R)
      \vspace{-1ex}
\begin{lstlisting}
$ /c/Program\ Files/R/R-x.y.z/bin/R.exe
\end{lstlisting}
      Windows avec Git Bash configuré pour R
      \vspace{-1ex}
\begin{lstlisting}
$ R
\end{lstlisting}
\end{minipage}
    \hfill
    \begin{minipage}[t]{0.3\linewidth}
      \small macOS
      \vspace{-1ex}
\begin{lstlisting}
$ R
\end{lstlisting}
    \end{minipage} \\
    \medskip
  \item Depuis l'invite de commande de R, déterminer le
    \alert{répertoire de travail} de R avec la commande \code{getwd()}
  \item Quitter R avec \code{q()}
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{\faCogs\; Prise en main (partie II)}

  \begin{minipage}{1.0\linewidth}
    \centering\large
    Lancer R (l'interface graphique) \\
    \bigskip
    \includegraphics[height=0.5\textheight,keepaspectratio]{images/Rlogo}
  \end{minipage}
  \hspace*{\fill}
  \begin{minipage}{0.15\linewidth}
    \centering\small
    (\textbf{pas} RStudio) \\
    \includegraphics[height=0.1\textheight,keepaspectratio]{images/RStudio}
  \end{minipage}
\end{frame}


\section{Bref historique}

\begin{frame}
  \frametitle{Origines}

  \begin{itemize}
  \item À l'origine fut le S au milieu des années 1970
  \item John~M.\ Chambers chez Bell Labs
  \item Principalement popularisé par mise en {\oe}uvre commerciale
    S-PLUS
  \end{itemize}
  \bigskip

  \begin{quote}
    \begin{minipage}{0.35\linewidth}
      \includegraphics[width=\linewidth,keepaspectratio]{images/Chambers}
    \end{minipage}
    \hfill
    \begin{minipage}{0.6\linewidth}
      \raggedright
      \textbf{ACM Software System Award 1998} \\
      \emph{John Chambers --- Langage S} \\[\baselineskip]

      «\dots\ which has forever altered how people analyse,
        visualize and manipulate data»
    \end{minipage}
  \end{quote}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{S redux}

  \begin{itemize}
  \item Nouvelle mise en {\oe}uvre du langage ---
    1990 --- \alert{R}oss Ihaka et \alert{R}obert Gentleman
  \item Inspirée de Scheme avec syntaxe du S \\
    \begin{minipage}{0.65\linewidth}
\begin{lstlisting}[language=lisp]
(define factorial (lambda (n)
  (if (= n 1)
      1
    (* n (factorial (- n 1))))))
\end{lstlisting}
    \end{minipage}
    \hfill
    \begin{minipage}{0.30\linewidth}
      syntaxe Scheme
    \end{minipage} \\
    vs \\
    \begin{minipage}{0.65\linewidth}
\begin{lstlisting}
factorial <- function(n)
  if (n == 1) 1 else n * factorial(n - 1)
\end{lstlisting}
    \end{minipage}
    \hfill
    \begin{minipage}{0.30\linewidth}
      syntaxe S
    \end{minipage}
  \item Libre («GNU S») et ouvert (CRAN) --- R surpasse S-PLUS
  \end{itemize}
\end{frame}


\section{Stratégie de travail}

\begin{frame}
  \frametitle{Anatomie d'une session de travail}

  «Le code est réel et les objets sont virtuels.»

  \begin{itemize}
  \item \emph{Console} R sert uniquement pour évaluer du code et
    observer les résultats
  \item Code sauvegardé dans des \alert{fichiers de script}
  \item Code (ré)évalué à la volée
    \begin{itemize}
    \item ligne par ligne de manière interactive
    \item par bloc logique
    \item en lot pour un fichier complet
    \end{itemize}
  \item Résultats de la session de travail \alert{non sauvegardés}
    (sauf exception)
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Répertoire de travail}

  Durant son exécution, l'interpréteur R garde les yeux rivés sur un
  seul répertoire de votre poste de travail: c'est le \alert{répertoire
    de travail de R}.

  \begin{minipage}[t]{0.45\linewidth}
    \renewcommand*{\DTstylecomment}{\normalfont}
    \dirtree{%
      .1 .
         .2 {\larger\faFolder}.
         .2 \textcolor{alert}{\larger\faFolderOpen} %
            \DTcomment{ répertoire de travail}.
            .3 \textcolor{code}{\larger\faFile*}.
            .3 \textcolor{code}{\larger\faFile*}.
            .3 \textcolor{code}{\larger\faFolderOpen}.
               .4 \textcolor{code}{\larger\faFile*}.
         .2 {\larger\faFolder}.
         .2 {\larger\faFolder}.  }\par
    \centering\smaller[2]
    \textcolor{code}{\rule[-0.1ex]{1.4ex}{1.4ex}}\, visible par R
  \end{minipage}
  \hfill\pause
  \begin{minipage}[t]{0.5\linewidth}
\begin{lstlisting}
> getwd()
[1] "/Users/vincent"
\end{lstlisting}
\begin{lstlisting}
> setwd("~/Documents")
> getwd()
[1] "/Users/vincent/Documents"
\end{lstlisting}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{\protect\faCogs\; Prise en main (partie III)}

  \begin{minipage}{1.0\linewidth}
    \centering\large
    Cette fois, lancer RStudio \\
    \bigskip
    \includegraphics[height=0.5\textheight,keepaspectratio]{images/RStudio}
  \end{minipage}

  \video{https://youtu.be/jX5QP0lWpF4}{Anatomie d'une session de
    travail avec RStudio}
\end{frame}

\begin{frame}[plain]
  \begin{columns}
    \begin{column}{0.45\textwidth}
      \tipbox{RStudio peut créer et éditer divers types de fichiers.}
    \end{column}
    \begin{column}{0.4\textwidth}
      \includegraphics[height=\textheight,keepaspectratio=true]{images/rstudio-new-file}
    \end{column}
  \end{columns}
\end{frame}


\section{Bonnes pratiques}

\begin{frame}
  \frametitle{Organisation des projets et des fichiers}

  Utiliser le système de fichiers de l'ordinateur pour classer les
  fichiers de script en projets.

  \begin{itemize}
  \item Regrouper tous les fichiers d'un projet dans un répertoire sur
    votre poste de travail
    \begin{itemize}
    \item un projet $=$ un répertoire
    \end{itemize}
  \item Faire de ce répertoire le répertoire de travail de R au
    lancement de la session
    \begin{itemize}
    \item via la commande \code{setwd}
    \item via le menu \code{Session|Set Working Directory} dans
      RStudio
    \end{itemize}
  \item Placer le code sous contrôle de versions
    \begin{itemize}
    \item Git ou un autre système de gestion de versions
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Programmer pour collaborer}

  Les fichiers de script doivent être \alert{portables}.

  \begin{itemize}
  \item Ne \alert{jamais} utiliser la commande \code{setwd} dans les
    fichiers de script
  \item Ne \alert{jamais} utiliser un chemin d'accès absolu (utiliser
    plutôt des chemins relatifs)
  \item Ne \alert{jamais} utiliser un chemin d'accès vers l'extérieur
    du projet
  \item Ne \alert{jamais} dépendre d'objets qui ne sont pas créés dans
    les scripts du projet
  \item \alert{Toujours} utiliser un codage de caractères universel
    (\link{https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange}{ASCII}
    ou \link{https://fr.wikipedia.org/wiki/UTF-8}{UTF-8})
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \centering
  \begin{minipage}{0.85\linewidth}
    \tipbox{L'outil de validation de
      \link{https://roger-project.gitlab.io/fr}{Roger} vérifie si les
      fichiers de script R sont portables.}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{\protect\faCogs\; Exercice}

  C'est le temps de vous lancer dans une première session de travail
  avec R.

  \begin{itemize}
  \item Le fichier de script \code{presentation.R} livré avec la
    formation contient du code qui passe en revue quelques unes des
    fonctionnalités de R.
  \item Ouvrez ce fichier dans RStudio, puis évaluez les lignes une
    à une en prenant soin de lire les commentaires et d'examiner les
    résultats.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{\protect\faWindows\; Avertissement pour les utilisateurs de
    Windows}

  Les caractères accentués n'apparaissent pas correctement dans les
  fichiers de script?
  \begin{itemize}
  \item Fichiers utilisent le codage de caractères
    \link{https://fr.wikipedia.org/wiki/UTF-8}{UTF-8}, qui n'est pas
    le codage par défaut sous Windows (uniquement)
  \item Après avoir ouvert le fichier dans RStudio, suivre le menu
    \code{File|Reopen with Encoding\dots} puis choisir UTF-8
    dans la liste
  \item Cocher la case \code{Set as default encoding for source files}
  \end{itemize}
\end{frame}

\section{Obtenir de l'aide}

\begin{frame}[plain]
  \tipbox{Consultez l'aide en ligne officielle de R. \newline
    (Plutôt que de vous fier aveuglément à Stack Overflow.)}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Obtenir de l'aide (suite)}

  Rubrique d'aide de l'objet \code{foo} accessible avec
\begin{lstlisting}
?foo
\end{lstlisting}
  ou
\begin{lstlisting}
help(foo)
\end{lstlisting}
  ou via les interfaces graphiques
\end{frame}

%% backmatter
\include{colophon}

\end{document}

%%% Local Variables:
%%% TeX-engine: xetex
%%% TeX-master: t
%%% coding: utf-8
%%% End:
